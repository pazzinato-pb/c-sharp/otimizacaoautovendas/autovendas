﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AutoVendas.Entities;
using AutoVendas.Operações;

namespace AutoVendas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Registro de Venda Mensal de Automóveis.");
            Console.WriteLine("---------------------------------------");

            Console.Write("Entre com o nome da concessionária: ");
            Validacoes.ValidarSeTextoEmBranco();
            string nomeDaConcessionaria = Validacoes.valorValidado;
            Console.WriteLine();

            Console.Write("Entre com a marca dos veículos: ");
            Validacoes.ValidarSeTextoEmBranco();
            string nomeDaMarca = Validacoes.valorValidado;
            Console.WriteLine();

            Console.Write("Quantos modelos serão cadastrados? ");
            Validacoes.ValidarNumeroInteiro();
            int numeroDeModelos = int.Parse(Validacoes.valorValidado);
            Console.WriteLine();

            Console.Write("Quantos funcionários serão cadastrados? ");
            Validacoes.ValidarNumeroInteiro();
            int numeroDeFuncionarios = int.Parse(Validacoes.valorValidado);
            Console.WriteLine();

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Cadastro de modelos:");
            Console.WriteLine("---------------------------------------");

            List<Modelo> listaDeModelos = new List<Modelo>();
            listaDeModelos = Cadastros.CadastrarModelos(listaDeModelos, numeroDeModelos);
            
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Cadastro de Funcionários: ");
            Console.WriteLine("---------------------------------------");


            List<Funcionario> listaDeFuncionarios = new List<Funcionario>();
            listaDeFuncionarios = Cadastros.CadastrarFuncionarios(listaDeFuncionarios, numeroDeFuncionarios);

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Registros de Vendas: ");
            Console.WriteLine("---------------------------------------");
            Console.WriteLine();
            
            Cadastros.RodarRotinaDeCadastroDeVendas(numeroDeModelos, numeroDeFuncionarios, listaDeFuncionarios, listaDeModelos);
            
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Relatório Mensal - Concessionaria {0} {1}: ", nomeDaMarca, nomeDaConcessionaria);
            Console.WriteLine();

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Vendas");
            Console.WriteLine("---------------------------------------");

            foreach (Modelo modelo in listaDeModelos)
            {
                Console.WriteLine("Quantidade de unidades vendidas do modelo {0}: {1} unidades", modelo.Nome, modelo.QuantidadeVendida);
                Console.WriteLine("Quantidade em estoque: {0} unidades", modelo.QuantidadeEmEstoque);
                Console.WriteLine();
            }


            Console.WriteLine();
            Console.WriteLine("Total de carros vendidos: {0} unidades.", Cadastros.quantidadeTotalDeVeiculos);
            Console.WriteLine();
            
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Salários:");
            Console.WriteLine("---------------------------------------");
            Console.WriteLine();

            foreach (Funcionario vendedor in listaDeFuncionarios)
            {
                Console.WriteLine("Comissão total recebida pelo(a) vendedor(a) {0} {1}: R${2}", vendedor.Nivel, vendedor.Nome, vendedor.CalcularComissaoTotal().ToString("F2", CultureInfo.InvariantCulture));
                Console.WriteLine("Salário total recebido pelo(a) vendedor(a) {0} {1}: R${2}", vendedor.Nivel, vendedor.Nome, vendedor.CalcularSalarioTotal().ToString("F2", CultureInfo.InvariantCulture));
                Console.WriteLine();
            }

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Renda: ");
            Console.WriteLine();
            if (Cadastros.lucroTotal >= 0)
            {
                Console.WriteLine("Lucro total: R${0}", Cadastros.lucroTotal.ToString("F2", CultureInfo.InvariantCulture));
            }
            else
            {
                Console.WriteLine("Prejuízo total: R${0}", Cadastros.lucroTotal.ToString("F2", CultureInfo.InvariantCulture));
            }
        }
        
    }
}
