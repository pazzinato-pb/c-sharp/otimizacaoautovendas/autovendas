﻿using AutoVendas.Entities;
using System.Collections.Generic;

namespace AutoVendas.Operações
{
    public class Selecoes
    {
        public static Modelo SelecionarModeloPorId(List<Modelo> listaDeModelos, int idDoModeloInformado)
        {
            Modelo modeloSelecionado = new Modelo();
            foreach (Modelo modeloCadastrado in listaDeModelos)
            {
                if (modeloCadastrado.Id == idDoModeloInformado)
                {
                    modeloSelecionado = modeloCadastrado;
                }
            }
            return modeloSelecionado;
        }

        public static Funcionario SelecionarFuncionarioPorId(List<Funcionario> listaDeFuncionarios, int idDoFuncionarioInformado)
        {
            Funcionario funcionarioSelecionado = new Funcionario();
            foreach (Funcionario funcionarioCadastrado in listaDeFuncionarios)
            {
                if (idDoFuncionarioInformado == funcionarioCadastrado.Id)
                {
                    funcionarioSelecionado = funcionarioCadastrado;
                }
            }
            return funcionarioSelecionado;
        }
    }
}
