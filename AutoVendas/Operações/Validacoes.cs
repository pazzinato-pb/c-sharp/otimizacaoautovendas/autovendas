﻿using AutoVendas.Entities;
using System;
using System.Collections.Generic;

namespace AutoVendas.Operações
{
    public class Validacoes
    {
        public static string valorValidado;

        public static void ValidarSeTextoEmBranco()
        {
            bool variavelParavalidacao = false;
            while (variavelParavalidacao == false)
            {
                string textoParaValidacao = Console.ReadLine();
                if (string.IsNullOrEmpty(textoParaValidacao) || string.IsNullOrWhiteSpace(textoParaValidacao))
                {
                    Console.Write("Texto em branco. Entre com nome válido: ");
                }
                else
                {
                    valorValidado = textoParaValidacao;
                    variavelParavalidacao = true;
                }
            }
        }

        public static void ValidarNumeroInteiro()
        {
            bool variavelParaAprovarValidacao = false;
            while (variavelParaAprovarValidacao == false)
            {
                string entradaParaValidacao = Console.ReadLine();
                int.TryParse(entradaParaValidacao, out int resultadoInteiro);
                if (resultadoInteiro == 0)
                {
                    Console.Write("Erro!! Entre com um valor numérico inteiro: ");
                    variavelParaAprovarValidacao = false;
                }
                else if (int.Parse(entradaParaValidacao) <= 0)
                {
                    Console.Write("Erro!! Entre com um valor positivo: ");
                    variavelParaAprovarValidacao = false;
                }
                else
                {
                   valorValidado = entradaParaValidacao;
                   variavelParaAprovarValidacao = true;
                }
            }
        }

        public static void ValidacaoDeNivelDoVendedor()
        {
            bool validacaoDeNivel = false;
            while (validacaoDeNivel == false)
            {
                string entradaDoNivelDoVendedor = Console.ReadLine();
                if (entradaDoNivelDoVendedor == "Aprendiz" || entradaDoNivelDoVendedor == "Lider" || entradaDoNivelDoVendedor == "Efetivo")
                {
                    valorValidado = entradaDoNivelDoVendedor;
                    validacaoDeNivel = true;
                }
                else
                {
                    Console.Write("O Valor não corresponde a nenhum nível. Informe se o funcionário é Aprendiz, Efetivo ou Lider: ");
                }
            }
        }

        public static void ValidarValorDecimal()
        {
            bool variavelParaAprovarValidacao = false;
            while (variavelParaAprovarValidacao == false)
            {
                string entradaParaValidacao = Console.ReadLine();
                decimal.TryParse(entradaParaValidacao, out decimal resultadoDecimal);
                if (resultadoDecimal == 0)
                {
                    Console.Write("Erro!! Entre com um valor numérico: ");
                    variavelParaAprovarValidacao = false;
                }
                else if (decimal.Parse(entradaParaValidacao) <= 0)
                {
                    Console.Write("Erro!! Entre com um valor positivo: ");
                    variavelParaAprovarValidacao = false;
                }
                else
                {
                    entradaParaValidacao = entradaParaValidacao.Replace(",", ".");
                    valorValidado = entradaParaValidacao;
                    variavelParaAprovarValidacao = true;
                }
            }
        }

        public static void ValidarSeExisteID(int maiorIdRegistrado)
        {
            bool variavelParaValidacaoSePossuiID = false;
            while (variavelParaValidacaoSePossuiID == false)
            {
                ValidarNumeroInteiro();
                if (int.Parse(valorValidado) <= maiorIdRegistrado && int.Parse(valorValidado) > 0)
                {
                    variavelParaValidacaoSePossuiID = true;
                }
                else
                {
                    Console.Write("Erro!! ID Inexistente. Entre com um ID válido: ");
                    variavelParaValidacaoSePossuiID = false;
                }
            }
        }

        public static void ValidarQuantidadeEmEstoque(int quantidadeInformada)
        {
            bool validacaoDeQuantidade = false;
            while (validacaoDeQuantidade == false)
            {
                ValidarNumeroInteiro();
                int quantidadesVendidasDoModelo = int.Parse(valorValidado);
                if (quantidadesVendidasDoModelo > quantidadeInformada)
                {
                    Console.Write("Valor é maior que a quantidade em estoque ({0} unidade(s)). Entre com um valor dentro do posssível: ", quantidadeInformada);
                }
                else
                {
                    validacaoDeQuantidade = true;
                }
            }
        }

        public static void ValidarSeExistemUnidades(List<Modelo> listaDeModelos, int numeroDeModelos)
        {
            bool validacaoSeExistemUnidadesDoModelo = false;
            while (validacaoSeExistemUnidadesDoModelo == false)
            {
                ValidarSeExisteID(numeroDeModelos);
                Modelo modeloSelecionado = Selecoes.SelecionarModeloPorId(listaDeModelos, int.Parse(valorValidado));
                if (modeloSelecionado.QuantidadeEmEstoque > 0)
                {
                    validacaoSeExistemUnidadesDoModelo = true;
                }
                else
                {
                    Console.WriteLine("Não existem unidades desse modelo em estoque. Selecione o ID de outro modelo: ");
                }
            }
        }
    }
}
