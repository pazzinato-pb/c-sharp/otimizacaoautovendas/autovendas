﻿using AutoVendas.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using AutoVendas.Entities.Enum;

namespace AutoVendas.Operações
{
    public class Cadastros
    {
        public static int quantidadeTotalDeVeiculos = 0;
        public static double lucroTotal = 0.0;
        public static void RodarRotinaDeCadastroDeVendas(int numeroDeModelos, int numeroDeFuncionarios, List<Funcionario> listaDeFuncionarios, List<Modelo> listaDeModelos)
        {
            Console.WriteLine("Registro de venda mensal: ");
            Console.WriteLine();
            bool validacaoDeLoop = true;
            while (validacaoDeLoop == true)
            {
                Console.Write("ID do funcionário responsável: ");
                Validacoes.ValidarSeExisteID(numeroDeFuncionarios);
                int idDoFuncionarioInformado = int.Parse(Validacoes.valorValidado);

                Console.Write("ID do modelo vendido: ");
                Validacoes.ValidarSeExistemUnidades(listaDeModelos, numeroDeModelos);
                int idDoModeloInformado = int.Parse(Validacoes.valorValidado);

                Modelo modeloSelecionado = Selecoes.SelecionarModeloPorId(listaDeModelos, idDoModeloInformado);

                Funcionario funcionarioSelecionado = Selecoes.SelecionarFuncionarioPorId(listaDeFuncionarios, idDoFuncionarioInformado);

                Console.Write("Quantidade de unidades vendidas: ");
                Validacoes.ValidarQuantidadeEmEstoque(modeloSelecionado.QuantidadeEmEstoque);
                int quantidadesVendidasDoModelo = int.Parse(Validacoes.valorValidado);
                Console.WriteLine();

                Venda vendaRealizada = new Venda(modeloSelecionado, quantidadesVendidasDoModelo);

                funcionarioSelecionado.AdicionarVenda(vendaRealizada);
                quantidadeTotalDeVeiculos += quantidadesVendidasDoModelo;
                modeloSelecionado.CalcularQuantidadeAposVenda(quantidadesVendidasDoModelo);
                lucroTotal += vendaRealizada.CalcularLucroTotalDaVenda();
                modeloSelecionado.CalcularQuantidadeVendida(quantidadesVendidasDoModelo);


                Console.Write("Registrar outra venda? (s/n) ");
                bool validacaoDeNovavenda = false;
                while (validacaoDeNovavenda == false)
                {
                    string leituraDeResposta = Console.ReadLine();

                    if (leituraDeResposta == "N" || leituraDeResposta == "n")
                    {
                        validacaoDeLoop = false;
                        validacaoDeNovavenda = true;
                    }
                    else if (leituraDeResposta == "S" || leituraDeResposta == "s")
                    {
                        validacaoDeLoop = true;
                        validacaoDeNovavenda = true;
                    }
                    else
                    {
                        Console.WriteLine("Valor inválido. Entre com 'S' ou 'N': ");
                    }
                }                
            }
        }

        public static List<Modelo> CadastrarModelos(List<Modelo> listaDeModelos, int numeroDeModelos)
        {
            for (int idDoModelo = 1; idDoModelo <= numeroDeModelos; idDoModelo++)
            {
                Console.WriteLine("Dados do modelo {0}", idDoModelo);

                Console.Write("Nome do modelo: ");
                Validacoes.ValidarSeTextoEmBranco();
                string nomeDoModelo = Validacoes.valorValidado; ;

                Console.Write("Quantidade em estoque: ");
                Validacoes.ValidarNumeroInteiro();
                int quantidadesDoModelo = int.Parse(Validacoes.valorValidado);

                Console.Write("Preço de Fábrica: ");
                Validacoes.ValidarValorDecimal();
                double precoDeFabricaDoModelo = double.Parse(Validacoes.valorValidado, CultureInfo.InvariantCulture);

                Console.Write("Preço de Venda: ");
                Validacoes.ValidarValorDecimal();
                double precoDeVendaDoModelo = double.Parse(Validacoes.valorValidado, CultureInfo.InvariantCulture);

                listaDeModelos.Add(new Modelo(idDoModelo, nomeDoModelo, quantidadesDoModelo, precoDeFabricaDoModelo, precoDeVendaDoModelo));
                Console.WriteLine();
            }
            return listaDeModelos;
        }

        public static List<Funcionario> CadastrarFuncionarios(List<Funcionario> listaDeFuncionarios, int numeroDeFuncionarios)
        {
            for (int idDoFuncionario = 1; idDoFuncionario <= numeroDeFuncionarios; idDoFuncionario++)
            {
                Console.WriteLine("Dados do funcionário {0}", idDoFuncionario);
                Console.Write("Nome do Vendedor: ");
                Validacoes.ValidarSeTextoEmBranco();
                string nome = Validacoes.valorValidado;

                Console.Write("Nivel (Aprendiz/Efetivo/Lider): ");
                Validacoes.ValidacaoDeNivelDoVendedor();
                ClassificacaoDoNivelDoVendedor classificacaoDoNivel = Enum.Parse<ClassificacaoDoNivelDoVendedor>(Validacoes.valorValidado);

                Console.Write("Salario base: ");
                Validacoes.ValidarValorDecimal();
                double salarioBase = double.Parse(Validacoes.valorValidado, CultureInfo.InvariantCulture);

                listaDeFuncionarios.Add(new Funcionario(idDoFuncionario, nome, classificacaoDoNivel, salarioBase));
                Console.WriteLine();
            }
            return listaDeFuncionarios;
        }
    }
}
