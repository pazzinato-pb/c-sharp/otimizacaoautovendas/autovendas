﻿namespace AutoVendas.Entities
{
    public class Venda
    {
        public Modelo ModeloParaVenda { get; set; }
        public int QuantidadeVendida { get; set; }
        
        public Venda()
        {
        }
        public Venda(Modelo modeloParaVenda, int quantidadeVendida)
        {
            ModeloParaVenda = modeloParaVenda;
            QuantidadeVendida = quantidadeVendida;
        }

        public double CalcularLucroTotalDaVenda()
        {
            return QuantidadeVendida * ModeloParaVenda.CalcularLucroPorUnidade();
        }
    }
}
