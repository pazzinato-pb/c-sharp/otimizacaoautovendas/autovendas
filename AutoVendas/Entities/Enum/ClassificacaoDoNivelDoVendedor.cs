﻿namespace AutoVendas.Entities.Enum
{
    public enum ClassificacaoDoNivelDoVendedor : int
    {
        Aprendiz = 0,
        Efetivo = 1,
        Lider = 2,
    }
}
