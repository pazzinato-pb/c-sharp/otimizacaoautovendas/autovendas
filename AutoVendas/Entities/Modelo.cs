﻿namespace AutoVendas.Entities
{
    public class Modelo
    {
        public int  Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeEmEstoque { get; set; }
        public double PrecoDeFabrica { get; set; }
        public double PrecoDeVenda { get; set; }
        public int  QuantidadeVendida { get; set; }

        public Modelo()
        {
        }

        public Modelo(int id, string nome, int quantidadeEmEstoque, double precoDeFabrica, double precoDeVenda)
        {
            Id = id;
            Nome = nome;
            QuantidadeEmEstoque = quantidadeEmEstoque;
            PrecoDeFabrica = precoDeFabrica;
            PrecoDeVenda = precoDeVenda;
        }

        public double CalcularLucroPorUnidade()
        {
            return PrecoDeVenda - PrecoDeFabrica;
        }
        public int CalcularQuantidadeVendida(int quantidadeVendida)
        {
            return QuantidadeVendida += quantidadeVendida;
        }

        public void CalcularQuantidadeAposVenda(int quantidadeVendida)
        {
            QuantidadeEmEstoque -= quantidadeVendida;
        }
    }
}
