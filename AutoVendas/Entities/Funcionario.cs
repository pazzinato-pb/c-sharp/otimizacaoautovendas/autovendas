﻿using System.Collections.Generic;
using AutoVendas.Entities.Enum;

namespace AutoVendas.Entities
{
    public class Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public ClassificacaoDoNivelDoVendedor Nivel { get; set; }
        public double SalarioBase { get; set; }
        public List<Venda> ListaDeVendas { get; set; } = new List<Venda>();

        public Funcionario()
        {
        }

        public Funcionario(int id, string nome, ClassificacaoDoNivelDoVendedor nivel, double salarioBase)
        {
            Id = id;
            Nome = nome;
            Nivel = nivel;
            SalarioBase = salarioBase;
        }

        public double SelecionarComissaoPorNivelDoVendedor()
        {
            double multiplicadorDeComissao = 0.0;
            switch (Nivel)
            {
                case ClassificacaoDoNivelDoVendedor.Aprendiz:
                    multiplicadorDeComissao = 0.015;
                    break;
                case ClassificacaoDoNivelDoVendedor.Efetivo:
                    multiplicadorDeComissao = 0.05;
                    break;
                case ClassificacaoDoNivelDoVendedor.Lider:
                    multiplicadorDeComissao = 0.1;
                    break;
            }

            return multiplicadorDeComissao;
        }

        
        public double CalcularSalarioTotal()
        {
            return CalcularComissaoTotal() + SalarioBase;
        }

        public double CalcularComissaoTotal()
        {
            return SomarVendasRealizadas() * SelecionarComissaoPorNivelDoVendedor();
        }

        public double SomarVendasRealizadas()
        {
            double valorTotalDasVendas = 0.0;
            foreach (Venda vendaRealizada in ListaDeVendas)
            {
                valorTotalDasVendas += vendaRealizada.CalcularLucroTotalDaVenda();
            }
            return valorTotalDasVendas;
        }


        public void AdicionarVenda(Venda vendaRealizada)
        {
            ListaDeVendas.Add(vendaRealizada);
        }
    }
}
